package com.hendisantika.submission2footballmacthschedule.adapter

import android.annotation.SuppressLint
import android.support.design.R.attr.colorPrimary
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.hendisantika.submission2footballmacthschedule.R
import com.hendisantika.submission2footballmacthschedule.R.id.*
import com.hendisantika.submission2footballmacthschedule.entity.Events
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import java.text.SimpleDateFormat

/**
 * Created by hendisantika on 21/10/18  07.02.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class MainAdapter(
    private val matches: List<Events>,
    private val listener: (Events) -> Unit
) : RecyclerView.Adapter<MatchViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MatchViewHolder {
        return MatchViewHolder(MatchesUi().createView(AnkoContext.create(parent.context, parent)))
    }

    override fun getItemCount(): Int = matches.size

    override fun onBindViewHolder(holder: MatchViewHolder, position: Int) {
        holder.bindItem(matches[position], listener)
    }
}

class MatchViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val homeTeamScore: TextView = view.find(tv_home_score)
    private val awayTeamName: TextView = view.find(tv_away_team)
    private val homeTeamName: TextView = view.find(tv_home_team)
    private val matchDate: TextView = view.find(tv_match_date)
    private val awayTeamScore: TextView = view.find(tv_away_score)

    @SuppressLint("SimpleDateFormat")
    fun bindItem(matches: Events, listener: (Events) -> Unit) {
        val formatedDate = SimpleDateFormat("dd/MM/yy").parse(matches.eventDate)
        val rDate = SimpleDateFormat("EE, dd-MM-yyyy")

        homeTeamName.text = matches.homeTeam
        homeTeamScore.text = matches.homeScore
        awayTeamName.text = matches.awayTeam
        awayTeamScore.text = matches.awayScore
        matchDate.text = rDate.format(formatedDate)

        itemView.setOnClickListener {
            listener(matches)
        }
    }
}

class MatchesUi : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {
            cardView {
                lparams(width = matchParent, height = dip(50)) {
                    topMargin = dip(5)
                    leftMargin = dip(5)
                    rightMargin = dip(5)
                }

                verticalLayout {
                    lparams(width = matchParent, height = wrapContent)
                    padding = dip(5)

                    textView {
                        id = R.id.tv_match_date
                        textColor = colorPrimary
                        this.gravity = Gravity.CENTER_HORIZONTAL
                    }.lparams(width = matchParent, height = wrapContent)

                    relativeLayout {
                        lparams(width = matchParent, height = wrapContent)

                        textView {
                            id = R.id.tv_home_team
                            textSize = 18F
                        }.lparams {
                            alignParentLeft()
                        }

                        textView {
                            id = R.id.tv_home_score
                            textSize = 18F
                        }.lparams {
                            leftOf(R.id.vs)
                        }

                        textView {
                            id = R.id.vs
                            text = resources.getString(R.string.vs)
                        }.lparams {
                            marginStart = dip(8)
                            marginEnd = dip(8)
                            centerHorizontally()
                        }

                        textView {
                            id = R.id.tv_away_score
                            textSize = 18F
                        }.lparams {
                            rightOf(R.id.vs)
                        }

                        textView {
                            id = R.id.tv_away_team
                            textSize = 18F
                        }.lparams {
                            alignParentRight()
                        }
                    }
                }
            }
        }
    }
}