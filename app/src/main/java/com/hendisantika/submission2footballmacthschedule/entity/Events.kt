package com.hendisantika.submission2footballmacthschedule.entity

import com.google.gson.annotations.SerializedName

/**
 * Created by hendisantika on 21/10/18  06.28.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
data class Events(
    @SerializedName("strHomeTeam") val homeTeam: String? = null,
    @SerializedName("strAwayTeam") val awayTeam: String? = null,
    @SerializedName("intHomeScore") val homeScore: String? = null,
    @SerializedName("intAwayScore") val awayScore: String? = null,
    @SerializedName("strHomeGoalDetails") val homeGoalDetails: String? = null,
    @SerializedName("strHomeRedCards") val homeRedCards: String? = null,
    @SerializedName("strHomeYellowCards") val homeYellowCards: String? = null,
    @SerializedName("strHomeLineupGoalkeeper") val homeLineupGoalkeeper: String? = null,
    @SerializedName("strHomeLineupDefense") val homeLineupDefense: String? = null,
    @SerializedName("strHomeLineupMidfield") val homeLineupMidfield: String? = null,
    @SerializedName("strHomeLineupForward") val homeLineupForward: String? = null,
    @SerializedName("strHomeLineupSubstitutes") val homeLineupSubstitutes: String? = null,
    @SerializedName("strAwayGoalDetails") val awayGoalDetails: String? = null,
    @SerializedName("strAwayRedCards") val awayRedCards: String? = null,
    @SerializedName("strAwayYellowCards") val awayYellowCards: String? = null,
    @SerializedName("strAwayLineupGoalkeeper") val awayLineupGoalkeeper: String? = null,
    @SerializedName("strAwayLineupDefense") val awayLineupDefense: String? = null,
    @SerializedName("strAwayLineupMidfield") val awayLineupMidfield: String? = null,
    @SerializedName("strAwayLineupForward") val awayLineupForward: String? = null,
    @SerializedName("strAwayLineupSubstitutes") val awayLineupSubstitutes: String? = null,
    @SerializedName("strDate") val eventDate: String? = null,
    @SerializedName("strTime") val eventTime: String? = null,
    @SerializedName("idHomeTeam") val idHomeTeam: String? = null,
    @SerializedName("idAwayTeam") val idAwayTeam: String? = null,
    @SerializedName("idEvent") val idEvent: String? = null
)