package com.hendisantika.submission2footballmacthschedule.entity

/**
 * Created by hendisantika on 21/10/18  06.31.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
data class Matches(val events: List<Events> = listOf())