package com.hendisantika.submission2footballmacthschedule.entity

/**
 * Created by hendisantika on 21/10/18  06.32.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
data class Team(val strTeamBadge: String? = null)