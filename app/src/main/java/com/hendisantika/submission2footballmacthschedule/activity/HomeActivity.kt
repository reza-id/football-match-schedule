package com.hendisantika.submission2footballmacthschedule.activity

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import com.hendisantika.submission2footballmacthschedule.R
import com.hendisantika.submission2footballmacthschedule.R.id.prev_match
import com.hendisantika.submission2footballmacthschedule.fragment.FavoriteMatchFragment
import com.hendisantika.submission2footballmacthschedule.fragment.NextMatchFragment
import com.hendisantika.submission2footballmacthschedule.fragment.PrevMatchFragment
import com.hendisantika.submission2footballmacthschedule.view.HomeActivityView
import org.jetbrains.anko.find
import org.jetbrains.anko.setContentView

class HomeActivity : AppCompatActivity() {

    private lateinit var bottomNav: BottomNavigationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        HomeActivityView().setContentView(this)

        bottomNav = find(R.id.bottomNavBar)
        bottomNav.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.prev_match -> {
                    loadPrevMatchFragment(savedInstanceState)
                }
                R.id.next_match -> {
                    loadNextMatchFragment(savedInstanceState)
                }
                R.id.favorites -> {
                    loadFavoritesFragment(savedInstanceState)
                }
            }
            false
        }
        bottomNav.selectedItemId = prev_match
    }

    private fun loadPrevMatchFragment(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.homeContainer, PrevMatchFragment(), PrevMatchFragment::class.java.simpleName)
                .commit()
        }
    }

    private fun loadNextMatchFragment(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.homeContainer, NextMatchFragment(), NextMatchFragment::class.java.simpleName)
                .commit()
        }
    }

    private fun loadFavoritesFragment(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.homeContainer, FavoriteMatchFragment(), FavoriteMatchFragment::class.java.simpleName)
                .commit()
        }
    }
}
