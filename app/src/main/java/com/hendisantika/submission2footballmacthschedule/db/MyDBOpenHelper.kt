package com.hendisantika.submission2footballmacthschedule.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*

/**
 * Created by hendisantika on 21/10/18  20.31.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class MyDBOpenHelper(ctx: Context) : ManagedSQLiteOpenHelper(ctx, "MyFavoriteMatch.db", null, 1) {

    companion object {
        private var instance: MyDBOpenHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): MyDBOpenHelper {
            if (instance == null) {
                instance = MyDBOpenHelper(ctx.applicationContext)
            }
            return instance as MyDBOpenHelper
        }
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.createTable(
            Favorite.TABLE_FAVORITE, true,
            Favorite.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
            Favorite.EVENT_ID to TEXT + UNIQUE,
            Favorite.EVENT_DATE to TEXT,
            Favorite.HOME_NAME to TEXT,
            Favorite.HOME_SCORE to TEXT,
            Favorite.AWAY_NAME to TEXT,
            Favorite.AWAY_SCORE to TEXT
        )
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.dropTable(Favorite.TABLE_FAVORITE, true)
    }
}

val Context.database: MyDBOpenHelper
    get() = MyDBOpenHelper.getInstance(applicationContext)