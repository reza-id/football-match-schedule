package com.hendisantika.submission2footballmacthschedule.db

/**
 * Created by hendisantika on 21/10/18  20.29.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
data class Favorite(
    val id: Long?,
    val eventId: String?,
    val eventDate: String?,
    val homeName: String?,
    val homeScore: String?,
    val awayName: String?,
    val awayScore: String?
) {

    companion object {
        const val TABLE_FAVORITE: String = "TABLE_FAVORITE"
        const val ID: String = "ID_"
        const val EVENT_ID: String = "EVENT_ID"
        const val EVENT_DATE: String = "EVENT_DATE"
        const val HOME_NAME: String = "HOME_NAME"
        const val HOME_SCORE: String = "HOME_SCORE"
        const val AWAY_NAME: String = "AWAY_NAME"
        const val AWAY_SCORE: String = "AWAY_SCORE"
    }
}