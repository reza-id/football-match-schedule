package com.hendisantika.submission2footballmacthschedule.view

import com.hendisantika.submission2footballmacthschedule.entity.Events

/**
 * Created by hendisantika on 21/10/18  06.47.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
interface MatchDetailsView {
    fun showLoading()

    fun hideLoading()

//    fun getHomeTeamBadge(data: List<Team>)
//
//    fun getAwayTeamBadge(data: List<Team>)
//
//    fun setHomeGoalDetail(goal: String)
//
//    fun setAwayGoalDetail(goal: String)
//
//    fun setHomeRedCards(card: String)
//
//    fun setAwayRedCards(card: String)
//
//    fun setHomeYellowCards(card: String)
//
//    fun setAwayYellowCards(card: String)
//
//    fun setDateTime(date: String)

    fun setEventData(data: List<Events>)

    fun setTeamBadge(home: String?, away: String?)
}