package com.hendisantika.submission2footballmacthschedule.view

import android.os.Build
import android.support.annotation.RequiresApi
import android.widget.LinearLayout
import com.hendisantika.submission2footballmacthschedule.R
import com.hendisantika.submission2footballmacthschedule.activity.HomeActivity
import org.jetbrains.anko.*
import org.jetbrains.anko.design.bottomNavigationView

/**
 * Created by hendisantika on 21/10/18  20.44.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class HomeActivityView : AnkoComponent<HomeActivity> {

    @RequiresApi(Build.VERSION_CODES.M)
    override fun createView(ui: AnkoContext<HomeActivity>) = with(ui) {
        relativeLayout {
            lparams(width = matchParent, height = matchParent)

            frameLayout {
                id = R.id.homeContainer
            }.lparams(width = matchParent, height = matchParent) {
                above(R.id.bottomLayout)
            }

            linearLayout {
                id = R.id.bottomLayout
                orientation = LinearLayout.VERTICAL

                view {
                    background = resources.getDrawable(R.drawable.shadow)
                }.lparams(width = matchParent, height = dip(4))

                bottomNavigationView {
                    inflateMenu(R.menu.main_menu)
                    id = R.id.bottomNavBar
                    itemBackgroundResource = R.color.colorBackground
                    itemIconTintList = resources.getColorStateList(android.R.color.white, context.theme)
                    itemTextColor = resources.getColorStateList(android.R.color.white, context.theme)
                }.lparams(width = matchParent, height = wrapContent)
            }.lparams(width = matchParent, height = wrapContent) {
                alignParentBottom()
            }
        }
    }
}