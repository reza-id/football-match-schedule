package com.hendisantika.submission2footballmacthschedule.view

import com.hendisantika.submission2footballmacthschedule.entity.Events

/**
 * Created by hendisantika on 21/10/18  06.41.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
interface MainView {
    fun showLoading()

    fun hideLoading()

    fun showMatchList(data: List<Events>)
}