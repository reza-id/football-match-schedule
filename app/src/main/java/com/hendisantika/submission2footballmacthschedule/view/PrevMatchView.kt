package com.hendisantika.submission2footballmacthschedule.view

import com.hendisantika.submission2footballmacthschedule.entity.Events

/**
 * Created by hendisantika on 21/10/18  20.51.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
interface PrevMatchView {
    fun showLoading()

    fun hideLoading()

    fun showMatchList(data: List<Events>)
}