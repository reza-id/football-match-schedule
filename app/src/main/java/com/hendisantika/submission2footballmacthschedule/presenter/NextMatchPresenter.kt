package com.hendisantika.submission2footballmacthschedule.presenter

import android.util.Log
import com.hendisantika.submission2footballmacthschedule.api.SportDBApi
import com.hendisantika.submission2footballmacthschedule.view.NextMatchView
import rx.Scheduler
import rx.schedulers.Schedulers
import rx.subscriptions.CompositeSubscription

/**
 * Created by hendisantika on 21/10/18  21.39.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class NextMatchPresenter(
    private val view: NextMatchView, private val sportDBApi: SportDBApi,
    private var scheduler: Scheduler
) {

    private val compositeSubs: CompositeSubscription = CompositeSubscription()

    fun getNextMatchesList(league: String) {
        view.showLoading()

        val subs = sportDBApi.getNextMatch(league)
            .subscribeOn(Schedulers.newThread())
            .observeOn(scheduler)
            .subscribe(
                { result ->
                    view.hideLoading()
                    view.showMatchList(result.events)
                },
                { error ->
                    Log.e("Error", error.message)
                }
            )

        compositeSubs.add(subs)
    }


    fun unSubscribe(){
        compositeSubs.unsubscribe()
    }
}