package com.hendisantika.submission2footballmacthschedule.presenter

import android.util.Log
import com.hendisantika.submission2footballmacthschedule.api.SportDBApi
import com.hendisantika.submission2footballmacthschedule.view.MatchDetailsView
import rx.Scheduler
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import rx.subscriptions.CompositeSubscription

/**
 * Created by hendisantika on 21/10/18  06.50.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class MatchDetailsPresenter(
    private val view: MatchDetailsView,
    private val sportDBApi: SportDBApi,
    private var scheduler: Scheduler
) {

    private val compositeSubs: CompositeSubscription = CompositeSubscription()

    fun getEventData(eventId: String) {
        view.showLoading()

        val subs = sportDBApi.getEvent(eventId)
            .subscribeOn(Schedulers.newThread())
            .observeOn(scheduler)
            .subscribe(
                { result ->
                    view.hideLoading()
                    view.setEventData(result.events)
                    getHomeTeamBadge(
                        result.events[0].idHomeTeam,
                        result.events[0].idAwayTeam
                    )
                },
                { error ->
                    Log.e("Error", error.message)
                }
            )

        compositeSubs.add(subs)
    }

    private fun getHomeTeamBadge(homeId: String?, awayId: String?) {
        view.showLoading()

        val subs = sportDBApi.getTeamDetails(homeId)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    view.hideLoading()
                    getAwayTeamBadge(result.teams[0].strTeamBadge, awayId)
                },
                { error ->
                    Log.e("Error", error.message)
                }
            )
        compositeSubs.add(subs)
    }

    private fun getAwayTeamBadge(homeBadge: String?, awayId: String?) {
        view.showLoading()

        val subs = sportDBApi.getTeamDetails(awayId)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    view.hideLoading()
                    view.setTeamBadge(homeBadge, result.teams[0].strTeamBadge)
                },
                { error ->
                    Log.e("Error", error.message)
                }
            )
        compositeSubs.add(subs)
    }

    fun unSubscribe(){
        compositeSubs.unsubscribe()
    }
}