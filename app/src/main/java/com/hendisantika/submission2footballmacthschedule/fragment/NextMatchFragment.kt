package com.hendisantika.submission2footballmacthschedule.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ProgressBar
import com.hendisantika.submission2footballmacthschedule.R
import com.hendisantika.submission2footballmacthschedule.activity.MatchDetailsActivity
import com.hendisantika.submission2footballmacthschedule.adapter.MainAdapter
import com.hendisantika.submission2footballmacthschedule.api.SportDBApi
import com.hendisantika.submission2footballmacthschedule.entity.Events
import com.hendisantika.submission2footballmacthschedule.presenter.NextMatchPresenter
import com.hendisantika.submission2footballmacthschedule.utils.invisible
import com.hendisantika.submission2footballmacthschedule.utils.visible
import com.hendisantika.submission2footballmacthschedule.view.NextMatchView
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.swipeRefreshLayout
import rx.android.schedulers.AndroidSchedulers

/**
 * Created by hendisantika on 21/10/18  21.37.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class NextMatchFragment : Fragment(), AnkoComponent<Context>, NextMatchView {

    private var matches: MutableList<Events> = mutableListOf()
    private lateinit var presenter: NextMatchPresenter
    private lateinit var adapter: MainAdapter
    private lateinit var progressBar: ProgressBar
    private lateinit var listMatch: RecyclerView
    private lateinit var swipeRefresh: SwipeRefreshLayout

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        adapter = MainAdapter(matches) {
            ctx.startActivity<MatchDetailsActivity>("id_event" to it.idEvent)
        }
        listMatch.adapter = adapter

        presenter = NextMatchPresenter(this, SportDBApi.OKHttpNich(), AndroidSchedulers.mainThread())
        presenter.getNextMatchesList("4328")
        swipeRefresh.onRefresh {
            presenter.getNextMatchesList("4328")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return createView(AnkoContext.create(ctx))
    }

    override fun createView(ui: AnkoContext<Context>): View = with(ui) {
        linearLayout {
            lparams(width = matchParent, height = matchParent)
            orientation = LinearLayout.VERTICAL
            topPadding = dip(8)
            leftPadding = dip(8)
            rightPadding = dip(8)

            swipeRefresh = swipeRefreshLayout {
                setColorSchemeResources(
                    R.color.colorAccent,
                    android.R.color.holo_green_light,
                    android.R.color.holo_orange_light,
                    android.R.color.holo_red_light
                )

                relativeLayout {
                    lparams(width = matchParent, height = matchParent)

                    listMatch = recyclerView {
                        lparams(width = matchParent, height = matchParent)
                        layoutManager = LinearLayoutManager(ctx)
                    }

                    progressBar = progressBar {
                    }.lparams {
                        centerInParent()
                    }
                }
            }
        }
    }

    override fun showLoading() {
        progressBar.visible()
    }

    override fun hideLoading() {
        progressBar.invisible()
    }

    override fun showMatchList(data: List<Events>) {
        swipeRefresh.isRefreshing = false
        matches.clear()
        matches.addAll(data)
        adapter.notifyDataSetChanged()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.unSubscribe()
    }
}