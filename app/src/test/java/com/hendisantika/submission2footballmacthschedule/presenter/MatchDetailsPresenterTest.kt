package com.hendisantika.submission2footballmacthschedule.presenter

import com.hendisantika.submission2footballmacthschedule.api.SportDBApi
import com.hendisantika.submission2footballmacthschedule.entity.Events
import com.hendisantika.submission2footballmacthschedule.entity.Matches
import com.hendisantika.submission2footballmacthschedule.view.MatchDetailsView
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import rx.Observable
import rx.schedulers.Schedulers


/**
 * Created by hendisantika on 27/10/18  09.05.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class MatchDetailsPresenterTest {

    @Mock
    private lateinit var view: MatchDetailsView

    @Mock
    private lateinit var sportDBApi: SportDBApi

    private lateinit var presenter: MatchDetailsPresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = MatchDetailsPresenter(view, sportDBApi, Schedulers.trampoline())
    }

    @Test
    fun getEventData() {
        val events: MutableList<Events> = mutableListOf()
        val matches = Matches(events)

        val eventId: String = "212"

        `when`(
            sportDBApi.getEvent(eventId)
        ).thenReturn(Observable.just(matches))

        presenter.getEventData(eventId)

        verify(view).showLoading()
        verify(view).hideLoading()
        verify(view).setEventData(events)



    }
}