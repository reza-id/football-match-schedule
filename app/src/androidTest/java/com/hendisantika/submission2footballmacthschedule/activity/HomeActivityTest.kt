package com.hendisantika.submission2footballmacthschedule.activity

import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.hendisantika.submission2footballmacthschedule.R
import org.junit.BeforeClass
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by hendisantika on 27/10/18  10.40.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */

@RunWith(AndroidJUnit4::class)
class HomeActivityTest {
    @Rule
    @JvmField
    var activityRule = ActivityTestRule(HomeActivity::class.java)

    companion object {
        @BeforeClass
        @JvmStatic
        fun beforeClass() {
            InstrumentationRegistry.getTargetContext().deleteDatabase("MyFavoriteMatch.db")
        }
    }

    @Test
    fun appBehaviourTest() {
        onView(withId(R.id.list_Match)).check(matches(isDisplayed()))

        Thread.sleep(2000)
        onView(withText("Sampdoria")).perform(click())
        onView(withText("GOAL")).check(matches(isDisplayed()))

        Thread.sleep(2000)
        onView(withId(R.id.add_to_favorite)).check(matches(isDisplayed()))
        onView(withId(R.id.add_to_favorite)).perform(click())
        onView(withText("Added to favorite")).check(matches(isDisplayed()))

        Espresso.pressBack()

        Thread.sleep(2000)
        onView(withId(R.id.favorites)).check(matches(isDisplayed()))
        onView(withId(R.id.favorites)).perform(click())
        onView(withText("Sampdoria")).check(matches(isDisplayed()))

        Thread.sleep(2000)
        onView(withText("Sampdoria")).perform(click())
        onView(withId(R.id.add_to_favorite)).check(matches(isDisplayed()))
        onView(withId(R.id.add_to_favorite)).perform(click())
        onView(withText("Removed from favorite")).check(matches(isDisplayed()))
    }
}